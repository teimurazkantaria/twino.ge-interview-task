Run project: **gradle bootRun**

Run tests: **gradle test**

Validation, security performed in Api layer, science 
In current implementation, application communicates with world with rest api,
but in the future, communicate may be done via socket, so instead placing
validation and security in controller layer, I placed it to the api (service) layer
to prevent code duplication and therefore,  possible security issues.

Domain Driven Design is used as approach of development

Onion application architecture is used: https://www.infoq.com/news/2014/10/ddd-onion-architecture

Updating of client field separated into different endpoints / methods,
So that I follow Task Driven User Interface.
Also, Task Driven UI fits better with Domain Driven Design principles
than typical CRUD UI.

@see http://kylecordes.com/2014/task-based-user-interfaces


## TODO ##
Not all code was covered by tests, no time.

No integration tests were implemented, due to lack of time.

Implement exception logging with ids
Implement exception serialization for rest endpoints with details. Also
map exceptions to appropriate HTTP STATUS codes.


## CURLs to test Api ##
(postman collection included in repo as well)

**1. Register new client:**

`curl -XPOST -H "Content-type: application/json" -d '{
   "name": "YOUR_NAME_HERE",
   "phoneNumber": "557391199",
   "birthDate": "1985-06-25",
   "monthlySalary": {
   	   "amount": 2000,
   	   "currency": "USD"
   },
   "remainingLiabilities": {
   	   "amount": 100,
   	   "currency": "USD"
   }
}' 'localhost:8080/api/auth/signup'`

**2. Login client**

`curl -XPOST -H "Content-type: application/json" -d '{
	"name": "Teimuraz"
}' 'localhost:8080/api/auth/login'`

**3. View client**

`curl -XGET 'http://localhost:8080/api/clients/ID_FROM_REGISTRATION_OR_LOGIN_RESPONSE'`
`curl -XGET 'http://localhost:8080/api/clients/1'`

**4. Change client name**

`curl -XPOST -H 'Authorization: Bearer ACCESS_TOKEN_FROM_REGISTRATION_OR_LOGIN_RESPONSE' -H "Content-type: application/json" -d '{
	"clientId": 1,
	"name": "Temo"
}' 'localhost:8080/api/clients/change-name'`

**5. Change client phone number**

`curl -XPOST -H 'Authorization: Bearer ACCESS_TOKEN_FROM_REGISTRATION_OR_LOGIN_RESPONSE' -H "Content-type: application/json" -d '{
	"clientId": 1,
	"phoneNumber": "557391199"
}' 'localhost:8080/api/clients/change-phone-number'`

**6. Change client monthly salary**

`curl -XPOST -H 'Authorization: Bearer ACCESS_TOKEN_FROM_REGISTRATION_OR_LOGIN_RESPONSE' -H "Content-type: application/json" -d '{
	"clientId": 1,
	"phoneNumber": "557391199"
}' 'localhost:8080/api/clients/change-phone-number'`


**7. Change client remaining liabilities**

`curl -XPOST -H 'Authorization: Bearer ACCESS_TOKEN_FROM_REGISTRATION_OR_LOGIN_RESPONSE' -H "Content-type: application/json" -d '{
	"clientId": 1,
	"phoneNumber": "557391199"
}' 'localhost:8080/api/clients/change-phone-number'`


## Client app ##
Usually, I manage frontend dependencies with bower of npm, also, grunt or gulp for concatenation / minimization, 
but now I just included necessary dependencies directly 