package twino.infrastructure;

import org.springframework.stereotype.Component;
import twino.domain.DomainConfiguration;
import twino.domain.DomainConfigurationProvider;

/**
 * In memory configuration implementation. DomainConfigurationProvider interface may be implemented as database configuration if needed.
 */
@Component
public class InMemoryDomainConfigurationProvider implements DomainConfigurationProvider {

    @Override
    public DomainConfiguration getConfig() {
        return new DomainConfiguration.Builder()
                .clientMinAge(21)
                .clientMaxAgeForCreditLimit(75)
                .build();
    }
}
