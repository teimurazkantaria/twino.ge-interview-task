package twino.infrastructure;

import twino.domain.Currency;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class JpaCurrencyConverter implements AttributeConverter<Currency, String> {

    @Override
    public String convertToDatabaseColumn(Currency attribute) {
        return attribute.toString();
    }

    @Override
    public Currency convertToEntityAttribute(String dbData) {
        return new Currency(dbData);
    }
}
