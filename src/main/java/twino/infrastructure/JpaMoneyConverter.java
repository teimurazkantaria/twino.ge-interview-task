package twino.infrastructure;

import twino.domain.Currency;
import twino.domain.Money;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter
public class JpaMoneyConverter implements AttributeConverter<Money, String> {

    @Override
    public String convertToDatabaseColumn(Money attribute) {
        return attribute.toString();
    }

    @Override
    public Money convertToEntityAttribute(String dbData) {
        String[] parts = dbData.split(" ");
        return new Money(new BigDecimal(parts[0]), new Currency(parts[1]));
    }
}
