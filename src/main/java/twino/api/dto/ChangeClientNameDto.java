package twino.api.dto;

import twino.api.validation.annotation.ClientName;

import javax.validation.constraints.NotNull;

public class ChangeClientNameDto {

    @NotNull
    private Long clientId;

    @NotNull
    @ClientName
    private String name;

    protected ChangeClientNameDto() {
    }

    public ChangeClientNameDto(Long clientId, String name) {
        this.clientId = clientId;
        this.name = name;
    }

    public Long getClientId() {
        return clientId;
    }

    public String getName() {
        return name;
    }
}
