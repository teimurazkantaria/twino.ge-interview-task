package twino.api.dto;

import javax.validation.constraints.NotNull;

public class ChangeClientPhoneNumberDto {

    @NotNull
    private Long clientId;

    @NotNull
    private String phoneNumber;

    protected ChangeClientPhoneNumberDto() {
    }

    public ChangeClientPhoneNumberDto(Long clientId, String phoneNumber) {
        this.clientId = clientId;
        this.phoneNumber = phoneNumber;
    }

    public Long getClientId() {
        return clientId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
