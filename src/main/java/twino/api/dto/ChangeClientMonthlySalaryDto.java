package twino.api.dto;

import twino.domain.Money;

import javax.validation.constraints.NotNull;

public class ChangeClientMonthlySalaryDto {

    @NotNull
    private Long clientId;

    @NotNull
    private Money monthlySalary;

    protected ChangeClientMonthlySalaryDto() {

    }

    public ChangeClientMonthlySalaryDto(Long clientId, Money monthlySalary) {
        this.clientId = clientId;
        this.monthlySalary = monthlySalary;
    }

    public Long getClientId() {
        return clientId;
    }

    public Money getMonthlySalary() {
        return monthlySalary;
    }
}
