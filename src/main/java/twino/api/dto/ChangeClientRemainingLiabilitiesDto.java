package twino.api.dto;

import twino.domain.Money;

import javax.validation.constraints.NotNull;

public class ChangeClientRemainingLiabilitiesDto {

    @NotNull
    private Long clientId;

    @NotNull
    private Money remainingLiabilities;

    protected ChangeClientRemainingLiabilitiesDto() {
    }

    public ChangeClientRemainingLiabilitiesDto(Long clientId, Money remainingLiabilities) {
        this.clientId = clientId;
        this.remainingLiabilities = remainingLiabilities;
    }

    public Long getClientId() {
        return clientId;
    }

    public Money getRemainingLiabilities() {
        return remainingLiabilities;
    }
}
