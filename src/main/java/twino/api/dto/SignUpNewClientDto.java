package twino.api.dto;

import org.joda.time.DateTime;
import twino.api.validation.annotation.ClientAge;
import twino.api.validation.annotation.ClientName;
import twino.domain.Money;

import javax.validation.constraints.NotNull;

public class SignUpNewClientDto {

    @NotNull
    @ClientName
    private String name;

    @NotNull
    private String phoneNumber;

    @NotNull
    @ClientAge
    private DateTime birthDate;

    @NotNull
    private Money monthlySalary;

    @NotNull
    private Money remainingLiabilities;

    public SignUpNewClientDto() {
    }

    private SignUpNewClientDto(Builder builder) {
        name = builder.name;
        phoneNumber = builder.phoneNumber;
        birthDate = builder.birthDate;
        monthlySalary = builder.monthlySalary;
        remainingLiabilities = builder.remainingLiabilities;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public Money getMonthlySalary() {
        return monthlySalary;
    }

    public Money getRemainingLiabilities() {
        return remainingLiabilities;
    }


    public static final class Builder {
        private String name;
        private String phoneNumber;
        private DateTime birthDate;
        private Money monthlySalary;
        private Money remainingLiabilities;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder phoneNumber(String val) {
            phoneNumber = val;
            return this;
        }

        public Builder birthDate(DateTime val) {
            birthDate = val;
            return this;
        }

        public Builder monthlySalary(Money val) {
            monthlySalary = val;
            return this;
        }

        public Builder remainingLiabilities(Money val) {
            remainingLiabilities = val;
            return this;
        }

        public SignUpNewClientDto build() {
            return new SignUpNewClientDto(this);
        }
    }
}
