package twino.api.event;

import twino.domain.Client;

public class NewClientSignedUpEvent {

    private final Client client;

    public NewClientSignedUpEvent(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewClientSignedUpEvent that = (NewClientSignedUpEvent) o;

        return client != null ? client.equals(that.client) : that.client == null;

    }

    @Override
    public int hashCode() {
        return client != null ? client.hashCode() : 0;
    }
}
