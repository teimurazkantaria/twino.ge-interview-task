package twino.api.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import twino.api.ClientProfileService;
import twino.api.dto.ChangeClientMonthlySalaryDto;
import twino.api.dto.ChangeClientNameDto;
import twino.api.dto.ChangeClientPhoneNumberDto;
import twino.api.dto.ChangeClientRemainingLiabilitiesDto;
import twino.api.error.ValidationException;
import twino.domain.Client;
import twino.domain.ClientRepository;

// TODO:: no time for tests

@Component
public class DefaultClientProfileService implements ClientProfileService {

    private ClientRepository clientRepository;

    @Autowired
    public DefaultClientProfileService(ClientRepository clientRepository) {
        Assert.notNull(clientRepository, "Client repository must not be null");

        this.clientRepository = clientRepository;
    }

    @Override
    public Client changeClientName(ChangeClientNameDto model) {
        Client client = clientRepository.findOne(model.getClientId());
        if (client == null) {
            throw new ValidationException("Client with id " + model.getClientId() + " does not exist");
        }

        client.changeName(model.getName());

        Client updatedClient = clientRepository.save(client);
        return updatedClient;
    }

    @Override
    public Client changeClientPhoneNumber(ChangeClientPhoneNumberDto model) {
        Client client = clientRepository.findOne(model.getClientId());
        if (client == null) {
            throw new ValidationException("Client with id " + model.getClientId() + " does not exist");
        }

        client.changePhoneNumber(model.getPhoneNumber());
        Client updatedClient = clientRepository.save(client);
        // TODO:: publish event
        return updatedClient;

    }

    @Override
    public Client changeClientMonthlySalary(ChangeClientMonthlySalaryDto model) {
        Client client = clientRepository.findOne(model.getClientId());
        if (client == null) {
            throw new ValidationException("Client with id " + model.getClientId() + " does not exist");
        }

        client.changeMonthlySalary(model.getMonthlySalary());
        Client updatedClient = clientRepository.save(client);
        // TODO:: publish event
        return updatedClient;
    }

    @Override
    public Client changeRemainingLiabilities(ChangeClientRemainingLiabilitiesDto model) {
        Client client = clientRepository.findOne(model.getClientId());
        if (client == null) {
            throw new ValidationException("Client with id " + model.getClientId() + " does not exist");
        }

        client.changeRemainingLiabilities(model.getRemainingLiabilities());
        Client updatedClient = clientRepository.save(client);
        // TODO:: publish event
        return updatedClient;
    }
}
