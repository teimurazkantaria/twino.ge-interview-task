package twino.api.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import twino.api.ClientViewerService;
import twino.core.error.ItemNotFoundException;
import twino.domain.Client;
import twino.domain.ClientRepository;

@Component
public class DefaultClientViewerService implements ClientViewerService {

    private ClientRepository clientRepository;

    @Autowired
    public DefaultClientViewerService(ClientRepository clientRepository) {
        Assert.notNull(clientRepository, "Client repository must not be null");

        this.clientRepository = clientRepository;
    }

    @Override
    public Client getClient(Long clientId) {
        Client client = clientRepository.findOne(clientId);
        if (client == null) {
            throw new ItemNotFoundException("Client with id " + clientId + " not found");
        }

        return client;
    }

    @Override
    public Iterable<Client> getAllClients() {
        return clientRepository.findAll();
    }
}
