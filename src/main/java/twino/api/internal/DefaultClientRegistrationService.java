package twino.api.internal;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import twino.api.ClientRegistrationService;
import twino.api.dto.SignUpNewClientDto;
import twino.api.event.NewClientSignedUpEvent;
import twino.domain.*;

@Component
public class DefaultClientRegistrationService implements ClientRegistrationService {

    private ClientRepository clientRepository;
    private CreditLimitCalculator creditLimitCalculator;
    private ApplicationEventPublisher eventPublisher;
    private DomainConfigurationProvider configurationProvider;

    @Autowired
    public DefaultClientRegistrationService(ClientRepository clientRepository,
                                            CreditLimitCalculator creditLimitCalculator,
                                            ApplicationEventPublisher eventPublisher,
                                            DomainConfigurationProvider configurationProvider) {
        Assert.notNull(clientRepository, "ApiConsumer repository must not be null");
        Assert.notNull(creditLimitCalculator, "Credit limit calculator must not be null");
        Assert.notNull(eventPublisher, "Event publisher must not be null");

        this.clientRepository = clientRepository;
        this.creditLimitCalculator = creditLimitCalculator;
        this.eventPublisher = eventPublisher;
        this.configurationProvider = configurationProvider;
    }

    @Override
    public Client signUpNewClient(SignUpNewClientDto dto) {
        Assert.notNull(dto, "Sign Up dto must not be null");

        Client client = new Client.Builder(configurationProvider)
                .name(dto.getName())
                .phoneNumber(dto.getPhoneNumber())
                .birthDate(dto.getBirthDate())
                .monthlySalary(dto.getMonthlySalary())
                .remainingLiabilities(dto.getRemainingLiabilities())
                .build();

        Money creditLimit = creditLimitCalculator.calculate(client.calculateAge(DateTime.now()), client.getMonthlySalary(), client.getRemainingLiabilities());

        client.changeCreditLimit(creditLimit);

        Client newClient = clientRepository.save(client);

        // Notify interested parties that new client signed up, for example, consumer may be mail or sms service which
        // sends welcome message or activation code to the client.
        eventPublisher.publishEvent(new NewClientSignedUpEvent(newClient));

        return newClient;
    }
}
