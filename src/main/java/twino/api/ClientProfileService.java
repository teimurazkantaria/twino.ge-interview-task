package twino.api;

import twino.api.dto.ChangeClientMonthlySalaryDto;
import twino.api.dto.ChangeClientNameDto;
import twino.api.dto.ChangeClientPhoneNumberDto;
import twino.api.dto.ChangeClientRemainingLiabilitiesDto;
import twino.domain.Client;

public interface ClientProfileService {

    /**
     * Change client name
     * @param dto
     */
    Client changeClientName(ChangeClientNameDto dto);

    /**
     * Change client phone number
     * @param dto
     */
    Client changeClientPhoneNumber(ChangeClientPhoneNumberDto dto);

    /**
     * Change client phone number
     * @param dto
     */
    Client changeClientMonthlySalary(ChangeClientMonthlySalaryDto dto);

    /**
     * Change remaining liabilities
     * @param dto
     */
    Client changeRemainingLiabilities(ChangeClientRemainingLiabilitiesDto dto);
}
