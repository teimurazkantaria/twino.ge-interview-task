package twino.api;

import twino.domain.Client;

public interface ClientViewerService {

    /**
     * Get client
     * @param clientId
     * @return
     */
    Client getClient(Long clientId);

    /**
     * Get all clients
     * @return
     */
    Iterable<Client> getAllClients();
}
