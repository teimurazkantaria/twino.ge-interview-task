package twino.api;

import twino.api.dto.SignUpNewClientDto;
import twino.domain.Client;

public interface ClientRegistrationService {

    /**
     * Sign Up new client.
     * @param dto
     * @return
     */
    Client signUpNewClient(SignUpNewClientDto dto);
}
