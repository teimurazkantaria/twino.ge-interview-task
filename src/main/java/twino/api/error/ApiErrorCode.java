package twino.api.error;

import twino.core.error.ErrorCode;

public class ApiErrorCode {

    public static ErrorCode create(Integer code) {
        return new ErrorCode("API_" + code);
    }

    public static final ErrorCode INVALID_DATA = create(1);
    public static final ErrorCode ROLE_NOT_FOUND = create(2);
    public static final ErrorCode ACCESS_TOKEN_IS_EMPTY = create(3);
    public static final ErrorCode INVALID_ACCESS_TOKEN = create(4);
    public static final ErrorCode ACCESS_TOKEN_EXPIRED = create(5);
    public static final ErrorCode ACCESS_TOKEN_USER_NOT_FOUND = create(6);
    public static final ErrorCode CANNOT_GENERATE_ACCESS_TOKEN = create(7);
}
