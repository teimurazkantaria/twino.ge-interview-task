package twino.api.error;

import twino.core.error.CoreException;
import twino.core.error.ErrorCode;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class ValidationException extends CoreException {

    private Set<? extends ConstraintViolation<Object>> constraintViolations;

    public ValidationException(String message) {
        super(message, null);
    }

    public ValidationException(String message, ErrorCode errorCode) {
        super(message, errorCode);
    }

    public ValidationException(String message, ErrorCode errorCode, Set<ConstraintViolation<Object>> constraintViolations) {
        this(message, errorCode);
        this.constraintViolations = constraintViolations;
    }

    public Set<? extends ConstraintViolation<Object>> getConstraintViolations() {
        return constraintViolations;
    }
}
