package twino.api.error;

import twino.core.error.CoreException;
import twino.core.error.ErrorCode;

public class ApiException extends CoreException {

    public ApiException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public ApiException(String message, ErrorCode errorCode) {
        super(message, errorCode, null);
    }

    public ApiException(String message) {
        super(message, null);
    }
}
