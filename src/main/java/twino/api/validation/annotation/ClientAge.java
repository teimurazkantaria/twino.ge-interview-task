package twino.api.validation.annotation;

import twino.api.validation.validator.ClientAgeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ClientAgeValidator.class)
@Documented
public @interface ClientAge {

    String message() default "Clients with your age not allowed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
