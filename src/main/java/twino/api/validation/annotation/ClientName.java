package twino.api.validation.annotation;

import twino.api.validation.validator.ClientNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ClientNameValidator.class)
@Documented
public @interface ClientName {

    String message() default "Client with provided name already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
