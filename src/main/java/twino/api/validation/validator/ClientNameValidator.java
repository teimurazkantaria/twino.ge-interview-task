package twino.api.validation.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twino.api.validation.annotation.ClientName;
import twino.domain.ClientRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ClientNameValidator implements ConstraintValidator<ClientName, String> {

    private ClientRepository clientRepository;

    @Autowired
    public ClientNameValidator(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public void initialize(ClientName constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }

        return clientRepository.findByName(value) == null;
    }
}
