package twino.api.validation.validator;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twino.api.validation.annotation.ClientAge;
import twino.domain.DomainConfigurationProvider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ClientAgeValidator implements ConstraintValidator<ClientAge, DateTime> {

    private DomainConfigurationProvider configurationProvider;

    @Autowired
    public ClientAgeValidator(DomainConfigurationProvider configurationProvider) {
        this.configurationProvider = configurationProvider;
    }

    @Override
    public void initialize(ClientAge constraintAnnotation) {
    }

    @Override
    public boolean isValid(DateTime value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }
        Period period = new Period(value, DateTime.now());
        int age = period.getYears();
        return age >= configurationProvider.getConfig().getClientMinAge();
    }
}
