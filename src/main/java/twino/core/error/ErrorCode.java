package twino.core.error;

import org.springframework.util.Assert;

import java.io.Serializable;

public class ErrorCode implements Serializable {

    private static final long serialVersionUID = -5085689463824279648L;

    private final String code;

    public ErrorCode(String code) {
        Assert.notNull(code, "Error code must not be null");
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorCode errorCode = (ErrorCode) o;

        return code != null ? code.equals(errorCode.code) : errorCode.code == null;

    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
