package twino.core.error;

public abstract class CoreException extends RuntimeException {

    private final ErrorCode errorCode;

    public CoreException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public CoreException(String message, ErrorCode errorCode) {
        this(message, errorCode, null);
    }

    public CoreException(String message) {
        this(message, null);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
