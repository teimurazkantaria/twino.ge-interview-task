package twino.core.error;

public class ItemNotFoundException extends CoreException {

    public ItemNotFoundException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public ItemNotFoundException(String message, ErrorCode errorCode) {
        super(message, errorCode, null);
    }

    public ItemNotFoundException(String message) {
        super(message, null);
    }
}
