package twino.core.error;

public class ForbiddenException extends CoreException {

    public ForbiddenException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public ForbiddenException(String message, ErrorCode errorCode) {
        super(message, errorCode, null);
    }

    public ForbiddenException(String message) {
        super(message, null);
    }
}
