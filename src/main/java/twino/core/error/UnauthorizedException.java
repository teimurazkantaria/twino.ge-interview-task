package twino.core.error;

public class UnauthorizedException extends CoreException {

    public UnauthorizedException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public UnauthorizedException(String message, ErrorCode errorCode) {
        super(message, errorCode, null);
    }

    public UnauthorizedException(String message) {
        super(message, null);
    }
}
