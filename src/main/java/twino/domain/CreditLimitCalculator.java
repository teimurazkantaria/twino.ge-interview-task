package twino.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import twino.domain.error.DomainException;

import java.math.BigDecimal;

/**
 * Credit limit calculator service.
 * Declared as class but not as an interface.
 * Domain services contain logic defined by business, which Credit Limit Calculator is, and not depend on any other layer,
 * so there is not reason to declare them as interface.
 *
 * @see <a href="http://stackoverflow.com/questions/2352654/should-domain-entities-be-exposed-as-interfaces-or-as-plain-objects">
 *      Stack overflow answer</a>
 *
 */
@Component
public class CreditLimitCalculator {

    private DomainConfigurationProvider domainConfigurationProvider;

    @Autowired
    public CreditLimitCalculator(DomainConfigurationProvider domainConfigurationProvider) {
        Assert.notNull(domainConfigurationProvider, "Domain configuration provider must not be null");

        this.domainConfigurationProvider = domainConfigurationProvider;
    }

    /**
     * Calculate credit limit.
     * @param age
     * @param monthlySalary
     * @param liabilities
     * @return
     */
    public Money calculate(Integer age, Money monthlySalary, Money liabilities) {
        Assert.notNull(age, "Age must not be null");
        Assert.notNull(monthlySalary, "Monthly salary must not be null");
        Assert.notNull(liabilities, "Liabilities must not be null");

        if (!monthlySalary.getCurrency().equals(liabilities.getCurrency())) {
            throw new DomainException("Monthly salary and liabilities currencies must match", DomainErrorCode.CURRENCIES_MISMATCH);
        }

        DomainConfiguration configuration = domainConfigurationProvider.getConfig();
        if (configuration == null) {
            throw new DomainException("Domain configuration not set", DomainErrorCode.DOMAIN_CONFIGURATION_NOT_SET);
        }

        if (age < configuration.getClientMinAge()) {
            throw new DomainException("ApiConsumer with age less then " + configuration.getClientMinAge() + " not allowed in the system", DomainErrorCode.LIMIT_CALCULATOR_BAD_AGE);
        }

        if (age > configuration.getClientMaxAgeForCreditLimit()) {
            return new Money(new BigDecimal(0), monthlySalary.getCurrency());
        }

        return (new Money(new BigDecimal(age * 100), monthlySalary.getCurrency()))
                .add(monthlySalary)
                .subtract(liabilities);
    }
}
