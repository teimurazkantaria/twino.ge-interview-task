package twino.domain;

import org.springframework.util.Assert;
import twino.domain.error.DomainException;
import twino.infrastructure.JpaCurrencyConverter;

import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Money Value Object
 */
@Embeddable
public class Money implements Serializable {

    private static final long serialVersionUID = -8760943850974142914L;

    private final BigDecimal amount;
    @Convert(converter = JpaCurrencyConverter.class)
    private final Currency currency;

    // For JPA
    protected Money() {
        this.amount = null;
        this.currency = null;
    }

    // @ConstructorProperties - used to bind json to this object, without this annotation binding fails.
    @ConstructorProperties({"amount", "currency"})
    public Money(BigDecimal amount, Currency currency) {
        Assert.notNull(amount, "Amount must not be null");
        Assert.notNull(currency, "Currency must not be null");

        this.amount = amount;
        this.currency = currency;
    }

    public Money add(Money money) {
        Assert.notNull(money, "Money must not be null");

        if (!getCurrency().equals(money.getCurrency())) {
            throw new DomainException("Money addition with different currencies not supported");
        }

        BigDecimal newAmount = getAmount().add(money.getAmount());
        return new Money(newAmount, getCurrency());
    }

    public Money subtract(Money money) {
        Assert.notNull(money, "Money must not be null");

        if (!getCurrency().equals(money.getCurrency())) {
            throw new DomainException("Money subtraction with different currencies not supported");
        }

        BigDecimal newAmount = getAmount().subtract(money.getAmount());
        return new Money(newAmount, getCurrency());
    }

    public Money multiply(Money money) {
        Assert.notNull(money, "Money param must not be null");

        if (!getCurrency().equals(money.getCurrency())) {
            throw new DomainException("Money multiplication with different currencies not supported");
        }

        BigDecimal newAmount = getAmount().multiply(money.getAmount());
        return new Money(newAmount, getCurrency());
    }

    public Money divide(Money money) {
        Assert.notNull(money, "Money param must not be null");

        if (!getCurrency().equals(money.getCurrency())) {
            throw new DomainException("Money division with different currencies not supported");
        }

        BigDecimal newAmount = getAmount().divide(money.getAmount());
        return new Money(newAmount, getCurrency());
    }

    @Override
    public String toString() {
        return amount + " " + currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        if (amount != null ? !amount.equals(money.amount) : money.amount != null) return false;
        return currency != null ? currency.equals(money.currency) : money.currency == null;

    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        return result;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
