package twino.domain.error;

import twino.core.error.CoreException;
import twino.core.error.ErrorCode;

public class DomainException extends CoreException {

    public DomainException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public DomainException(String message, ErrorCode errorCode) {
        super(message, errorCode, null);
    }

    public DomainException(String message) {
        super(message, null);
    }
}
