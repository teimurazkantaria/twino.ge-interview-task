package twino.domain;

import twino.core.error.ErrorCode;

public class DomainErrorCode {

    public static ErrorCode create(Integer code) {
        return new ErrorCode("DOMAIN_" + code);
    }

    public static final ErrorCode AGE_CALC_TO_DATE_LESS_THAN_BIRTH_DATE = create(1);
    public static final ErrorCode LIMIT_CALCULATOR_BAD_AGE = create(2);
    public static final ErrorCode DOMAIN_CONFIGURATION_NOT_SET = create(3);
    public static final ErrorCode CURRENCIES_MISMATCH = create(4);
}
