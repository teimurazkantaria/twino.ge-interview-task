package twino.domain;

/**
 * Holds domain configuration
 */
public class DomainConfiguration {

    /**
     * Min age which for registration
     */
    private final Integer clientMinAge;

    /**
     * Max age allowed for credit limit
     */
    private final Integer clientMaxAgeForCreditLimit;

    public DomainConfiguration(Integer clientMinAge, Integer clientMaxAgeForCreditLimit) {
        this.clientMinAge = clientMinAge;
        this.clientMaxAgeForCreditLimit = clientMaxAgeForCreditLimit;
    }

    private DomainConfiguration(Builder builder) {
        clientMinAge = builder.clientMinAge;
        clientMaxAgeForCreditLimit = builder.clientMaxAgeForCreditLimit;
    }

    public Integer getClientMinAge() {
        return clientMinAge;
    }

    public Integer getClientMaxAgeForCreditLimit() {
        return clientMaxAgeForCreditLimit;
    }


    public static final class Builder {
        private Integer clientMinAge;
        private Integer clientMaxAgeForCreditLimit;

        public Builder() {
        }

        public Builder clientMinAge(Integer val) {
            clientMinAge = val;
            return this;
        }

        public Builder clientMaxAgeForCreditLimit(Integer val) {
            clientMaxAgeForCreditLimit = val;
            return this;
        }

        public DomainConfiguration build() {
            return new DomainConfiguration(this);
        }
    }
}
