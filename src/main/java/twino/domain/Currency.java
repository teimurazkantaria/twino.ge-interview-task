package twino.domain;

import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * Currency Value Object
 */
public class Currency implements Serializable {

    public static Currency USD = new Currency("USD");
    public static Currency EUR = new Currency("EUR");
    public static Currency GEL = new Currency("GEL");

    private static final long serialVersionUID = -4151367914261402646L;

    private final String code;

    // @ConstructorProperties - used to bind json to this object, without this annotation binding fails.
    public Currency(String code) {
        Assert.notNull(code, "Currency code must not be null");

        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        return code != null ? code.equals(currency.code) : currency.code == null;

    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }

}
