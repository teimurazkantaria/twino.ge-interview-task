package twino.domain;

import org.springframework.data.repository.CrudRepository;

/**
 * ApiConsumer repository
 */
public interface ClientRepository extends CrudRepository<Client, Long> {

    Client findByName(String name);
}
