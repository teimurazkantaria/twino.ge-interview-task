package twino.domain;

/**
 * Domain configuration provider
 */
public interface DomainConfigurationProvider {

    DomainConfiguration getConfig();
}
