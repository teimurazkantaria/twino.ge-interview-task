package twino.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.util.Assert;
import twino.domain.error.DomainException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Client Aggregate Root
 */
@Entity
public class Client implements User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String phoneNumber;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private DateTime birthDate;

    @NotNull
    @AttributeOverrides( {
            @AttributeOverride(name="amount", column = @Column(name="monthly_salary_amount")),
            @AttributeOverride(name="currency", column = @Column(name="monthly_salary_currency"))
    })
    private Money monthlySalary;

    @NotNull
    @AttributeOverrides( {
            @AttributeOverride(name="amount", column = @Column(name="remaining_liabilities_amount")),
            @AttributeOverride(name="currency", column = @Column(name="remaining_liabilities_currency"))
    })
    private Money remainingLiabilities;

    private String avatar;

    @NotNull
    @AttributeOverrides( {
            @AttributeOverride(name="amount", column = @Column(name="credit_limit_amount")),
            @AttributeOverride(name="currency", column = @Column(name="credit_limit_currency"))
    })
    private Money creditLimit;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private DateTime createdAt;

    private  DomainConfigurationProvider configurationProvider;

    protected Client() {
    }

    private Client(Builder builder) {
        setConfigurationProvider(builder.configurationProvider);
        setId(builder.id);
        setName(builder.name);
        setPhoneNumber(builder.phoneNumber);
        setBirthDate(builder.birthDate);
        setMonthlySalary(builder.monthlySalary);
        setRemainingLiabilities(builder.remainingLiabilities);
        setAvatar(builder.avatar);
        setCreditLimit(builder.creditLimit);
        setCreatedAt(builder.createdAt);
    }

    /**
     * Get age in years
     * @return
     */
    public int calculateAge(DateTime toDate) {
        Assert.notNull(toDate);

        return calculateAge(birthDate, toDate);
    }

    private int calculateAge(DateTime birthDate, DateTime toDate) {
        Assert.notNull(birthDate);
        Assert.notNull(toDate);

        Period period = new Period(birthDate, toDate);

        if (toDate.isBefore(birthDate)) {
            throw new DomainException("To Date must be greater or equal to Birth Date", DomainErrorCode.AGE_CALC_TO_DATE_LESS_THAN_BIRTH_DATE);
        }
        return period.getYears();
    }

    // DDD principles do not encourage to have setters in entities public api, instead,
    // everything in domain api should follow UBIQUITOUS LANGUAGE that reflects purpose of action.
    // In DDD, one should stay away from commonplaces, which setters are.
    // In current implementation, change... methods do not contain any complex logic, for now they just
    // call appropriate setters.
    /**
     * Change credit limit.
     * @param creditLimit
     */
    public void changeCreditLimit(Money creditLimit) {
        Assert.notNull(creditLimit, "Credit limit must not be null");

        this.setCreditLimit(creditLimit);
    }

    /**
     * Change name
     * @param name
     */
    public void changeName(String name) {
        this.setName(name);
    }

    /**
     * Change phone number
     * @param phoneNumber
     */
    public void changePhoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
    }

    /**
     * Change monthly salary
     * @param monthlySalary
     */
    public void changeMonthlySalary(Money monthlySalary) {
        this.setMonthlySalary(monthlySalary);
    }

    /**
     * Change remaining liabilities
     * @param remainingLiabilities
     */
    public void changeRemainingLiabilities(Money remainingLiabilities) {
        this.setRemainingLiabilities(remainingLiabilities);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public Money getMonthlySalary() {
        return monthlySalary;
    }

    public Money getRemainingLiabilities() {
        return remainingLiabilities;
    }

    public String getAvatar() {
        return avatar;
    }

    public Money getCreditLimit() {
        return creditLimit;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    // Make setters private, only for internal use. DDD principles do not encourage to have setters in entities public api.
    private void setId(Long id) {
        this.id = id;
    }

    private void setName(String name) {
        Assert.notNull(name, "Name  must not be null");

        this.name = name;
    }

    private void setPhoneNumber(String phoneNumber) {
        Assert.notNull(phoneNumber, "Phone number must not be null");

        this.phoneNumber = phoneNumber;
    }

    private void setBirthDate(DateTime birthDate) {
        Assert.notNull(birthDate, "Birth date must not be null");
        // Check age for sure, if, for example, validation missed somwhere

        int age = calculateAge(birthDate, DateTime.now());
        if (age < configurationProvider.getConfig().getClientMinAge()) {
            throw new DomainException("Client with age under " + age + " not allowed");
        }

        this.birthDate = birthDate;
    }

    private void setMonthlySalary(Money monthlySalary) {
        Assert.notNull(monthlySalary, "Monthly salary must not be null");

        this.monthlySalary = monthlySalary;
    }

    private void setRemainingLiabilities(Money remainingLiabilities) {
        Assert.notNull(remainingLiabilities, "Remaining liabilities must not be null");

        this.remainingLiabilities = remainingLiabilities;
    }

    private void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    private void setCreditLimit(Money creditLimit) {

        this.creditLimit = creditLimit;
    }

    private void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    private void setConfigurationProvider(DomainConfigurationProvider configurationProvider) {
        Assert.notNull(configurationProvider, "Configuration provider should not be null");

        this.configurationProvider = configurationProvider;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String phoneNumber;
        private DateTime birthDate;
        private Money monthlySalary;
        private Money remainingLiabilities;
        private String avatar;
        private Money creditLimit;
        private DateTime createdAt;
        private DomainConfigurationProvider configurationProvider;

        public Builder(DomainConfigurationProvider configurationProvider) {
            this.configurationProvider = configurationProvider;
        }

        public Builder(Client copy) {
            this.id = copy.id;
            this.name = copy.name;
            this.phoneNumber = copy.phoneNumber;
            this.birthDate = copy.birthDate;
            this.monthlySalary = copy.monthlySalary;
            this.remainingLiabilities = copy.remainingLiabilities;
            this.avatar = copy.avatar;
            this.creditLimit = copy.creditLimit;
            this.createdAt = copy.createdAt;
            this.configurationProvider = copy.configurationProvider;
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder phoneNumber(String val) {
            phoneNumber = val;
            return this;
        }

        public Builder birthDate(DateTime val) {
            birthDate = val;
            return this;
        }

        public Builder monthlySalary(Money val) {
            monthlySalary = val;
            return this;
        }

        public Builder remainingLiabilities(Money val) {
            remainingLiabilities = val;
            return this;
        }

        public Builder avatar(String val) {
            avatar = val;
            return this;
        }

        public Builder creditLimit(Money val) {
            creditLimit = val;
            return this;
        }

        public Builder createdAt(DateTime val) {
            createdAt = val;
            return this;
        }

        public Client build() {
            return new Client(this);
        }
    }
}

