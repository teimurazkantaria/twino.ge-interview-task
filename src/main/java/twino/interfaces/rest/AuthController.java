package twino.interfaces.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import twino.api.ClientRegistrationService;
import twino.api.dto.SignUpNewClientDto;
import twino.domain.Client;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private ClientRegistrationService clientRegistrationService;

    @Autowired
    public AuthController(ClientRegistrationService clientRegistrationService) {
        this.clientRegistrationService = clientRegistrationService;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public Client signUp(@RequestBody @Valid SignUpNewClientDto dto, HttpServletRequest request) {
        Client client = clientRegistrationService.signUpNewClient(dto);
        return client;
    }
}
