package twino.interfaces.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import twino.api.*;
import twino.api.dto.ChangeClientMonthlySalaryDto;
import twino.api.dto.ChangeClientNameDto;
import twino.api.dto.ChangeClientPhoneNumberDto;
import twino.api.dto.ChangeClientRemainingLiabilitiesDto;
import twino.domain.Client;

/**
 * Updating of client field separated into different endpoints / method,
 * So that I follow Task Driven User Interface.
 * Also, Task Driven UI fits better with Domain Driven Design principles
 * than typical CRUD UI.
 *
 * @see http://kylecordes.com/2014/task-based-user-interfaces
 */
@RestController
@RequestMapping("/api/clients")
public class ClientsController {

    private ClientProfileService clientProfileService;
    private ClientViewerService clientViewerService;

    @Autowired
    public ClientsController(ClientProfileService clientProfileService, ClientViewerService clientViewerService) {
        Assert.notNull(clientProfileService, "Client profile service must not be null");
        Assert.notNull(clientViewerService, "Client viewer service must not be null");

        this.clientProfileService = clientProfileService;
        this.clientViewerService = clientViewerService;
    }

    @RequestMapping("/{id}")
    public Client get(@PathVariable Long id) {
        return clientViewerService.getClient(id);
    }

    @RequestMapping(value = "/change-name", method = RequestMethod.POST)
    public Client changeName(@RequestBody ChangeClientNameDto dto) {
        return clientProfileService.changeClientName(dto);
    }

    @RequestMapping(value = "/change-phone-number", method = RequestMethod.POST)
    public Client changePhoneNumber(@RequestBody ChangeClientPhoneNumberDto dto) {
        return clientProfileService.changeClientPhoneNumber(dto);
    }

    @RequestMapping(value = "/change-monthly-salary", method = RequestMethod.POST)
    public Client changeMonthlySalary(@RequestBody ChangeClientMonthlySalaryDto dto) {
        return clientProfileService.changeClientMonthlySalary(dto);
    }

    @RequestMapping(value = "/change-remaining-liabilities", method = RequestMethod.POST)
    public Client changeRemainingLiabilities(@RequestBody ChangeClientRemainingLiabilitiesDto dto) {
        return clientProfileService.changeRemainingLiabilities(dto);
    }

    @RequestMapping("/")
    public Iterable<Client> getClients() {
        return clientViewerService.getAllClients();
    }
}
