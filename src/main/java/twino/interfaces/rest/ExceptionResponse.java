package twino.interfaces.rest;

import java.util.UUID;

public class ExceptionResponse {

    private UUID id;
    private String message;
    private String errorCode;

    public ExceptionResponse(UUID id, String message, String errorCode) {
        this.id = id;
        this.message = message;
        this.errorCode = errorCode;
    }

    public UUID getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }
}