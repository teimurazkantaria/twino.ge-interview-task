package twino.interfaces.rest;

import flexjson.JSONSerializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import twino.api.error.ValidationException;
import twino.core.error.CoreException;
import twino.core.error.ErrorCode;
import twino.core.error.ForbiddenException;
import twino.core.error.UnauthorizedException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

// TODO:: Log each exception with exception id so it will be easier do debug user's issues
@ControllerAdvice
public class HttpExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<?> handle(ValidationException e) {
        return new ResponseEntity(new InvalidDataExceptionResponse(
                generateExceptonId(),
                e.getMessage(),
                e.getErrorCode() != null ? e.getErrorCode().getCode() : "Not set",
                e.getConstraintViolations()),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<?> handle(ForbiddenException e) {
        return new ResponseEntity(new ExceptionResponse(
                generateExceptonId(),
                e.getMessage(),
                e.getErrorCode() != null ? e.getErrorCode().getCode() : "Not set"),
                HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<?> handle(UnauthorizedException e) {
        return new ResponseEntity(new ExceptionResponse(
                generateExceptonId(),
                e.getMessage(),
                e.getErrorCode() != null ? e.getErrorCode().getCode() : "Not set"),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(CoreException.class)
    public ResponseEntity<?> handle(CoreException e) {
        return new ResponseEntity(new ExceptionResponse(
                generateExceptonId(),
                e.getMessage(),
                e.getErrorCode() != null ? e.getErrorCode().getCode() : "Not set"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handle(Exception e) throws Exception {
        UUID id = UUID.randomUUID();
        ErrorCode errorCode = new ErrorCode("Server error");
        String message = "Server error";
        String exceptionClass = e.getClass().getName();
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        Map<String, Object> log = getExceptionJsonObject(id, message, exceptionClass, httpStatus, errorCode, e.getStackTrace());
        try {
            JSONSerializer serializer = new JSONSerializer();
            String json = serializer.prettyPrint(true).deepSerialize(log);
            // Log json error
        }
        catch (Exception serializerException) {
            // logger.error("Could not serialize data. "+serializerException.getMessage());
        }
        throw e;
    }

    private UUID generateExceptonId() {
        return UUID.randomUUID();
    }

    private Map<String, Object> getExceptionJsonObject(UUID id, String message, String exceptionClass, HttpStatus httpStatus, ErrorCode errorCode, StackTraceElement[] stackTrace) {
        Map<String, Object> exceptionObject = new HashMap<>();
        exceptionObject.put("Id", id);
        exceptionObject.put("Message", message);
        exceptionObject.put("Exception Class", exceptionClass);
        exceptionObject.put("Error Code", errorCode != null ? errorCode.getCode() : "Not set");


        StringBuilder stringBuilder = new StringBuilder();
        for (StackTraceElement element : stackTrace) {
            stringBuilder.append(element.toString());
            stringBuilder.append(" // ");
        }

        exceptionObject.put("Stacktrace", stringBuilder.toString());
        return exceptionObject;
    }
}

