package twino.interfaces.rest;


import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.UUID;

public class InvalidDataExceptionResponse extends ExceptionResponse {

    private Set<? extends ConstraintViolation<Object>> violations;

    public InvalidDataExceptionResponse(UUID id, String message, String errorCode, Set<? extends ConstraintViolation<Object>> violations) {
        super(id, message, errorCode);
        this.violations = violations;
    }

    public Set<? extends ConstraintViolation<Object>> getViolations() {
        return violations;
    }

    public void setViolations(Set<? extends ConstraintViolation<Object>>violations) {
        this.violations = violations;
    }
}
