(function(){
    "use strict";
    angular
        .module("twino")
        .controller('ProfileCtrl', function(app, $scope, $location, $http, settings){
            app.activeController = "profile";
            if (!app.apiConsumer) {
                $location.path('/');
            }

            $scope.client = {};
            $scope.apiConsumer = app.apiConsumer;
            if ($scope.apiConsumer) {
                $scope.client = $scope.apiConsumer.user;
            }

            $scope.fieldsChanging = {};

            $scope.changeField = function (field) {
                console.log('skdjdsk');
                $scope.fieldsChanging[field] = true;
            }

            $scope.saveField = function (field) {
                $scope.fieldsChanging[field] = false;
                var fieldToUrlPathMapping = {
                    name: 'name',
                    phoneNumber: 'phone-number',
                    birthDate: 'birth-date',
                    monthlySalary: 'monthly-salary',
                    remainingLiabilities: 'remaining-liabilities'
                };

                var data = {
                    clientId: $scope.client.id,
                }

                data[field] = $scope.client[field];

                $http({
                    method: 'POST',
                    url: settings.apiUrl + '/clients/change-'+fieldToUrlPathMapping[field],
                    responseType: 'json',
                    data: data,
                    headers: {
                        "Authorization": "Bearer " + app.apiConsumer.accessToken.token,
                    }
                }).then(
                    /**
                     * Success callback
                     * @param response
                     */
                    function (response) {
                        console.log(response);
                    },
                    /**
                     * Error callback
                     * @param response
                     */
                    function (response) {
                        console.log('error');
                        console.log(response);
                    }
                )
            }

            $scope.cancelFieldChange = function (field) {
                $scope.fieldsChanging[field] = false;
            }
        });


})();