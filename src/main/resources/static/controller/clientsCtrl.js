(function(){
    "use strict";
    angular
        .module("twino")
        .controller('ClientsCtrl', function(app, $scope, $location, $http, settings){
            app.activeController = "clients";

            $scope.clients = [];

            $http({
                method: 'GET',
                url: settings.apiUrl + '/clients/',
                responseType: 'json',
            }).then(
                /**
                 * Success callback
                 * @param response
                 */
                function (response) {
                    console.log(response);
                    $scope.clients = response.data;
                },
                /**
                 * Error callback
                 * @param response
                 */
                function (response) {
                    console.log('error');
                    console.log(response);
                }
            )
        });
})();