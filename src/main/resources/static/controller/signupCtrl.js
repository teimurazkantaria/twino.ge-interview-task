(function(){
    "use strict";
    angular
        .module("twino")
        .controller('SignupCtrl', function(app, $scope, $location, $http, settings){
            app.activeController = "signup";

            $scope.clientData = {
                name: 'Teimuraz',
                phoneNumber: '557391199',
                birthDate: '1985-06-25',
                monthlySalary: 10000,
                remainingLiabilities: 100
            };
            $scope.submitAttempt = false;

            /**
             * Signup
             * @param customer
             * @return {boolean}
             */
            $scope.signup = function(clientData) {
                $scope.submitAttempt = true;

                if (!$scope.signupForm.$valid) {
                    return false;
                }

                var clientDataFormated = angular.copy(clientData);
                clientDataFormated.monthlySalary = {
                    amount: clientDataFormated.monthlySalary,
                    currency: 'USD'
                };

                clientDataFormated.remainingLiabilities = {
                    amount: clientDataFormated.remainingLiabilities,
                    currency: 'USD'
                };

                app.error.message = '';
                $http({
                    method: 'POST',
                    url: settings.apiUrl + '/auth/signup',
                    responseType: 'json',
                    data: clientDataFormated
                }).then(
                    /**
                     * Success callback
                     * @param response
                     */
                    function (response) {
                        app.apiConsumer = response.data;
                        $location.path("/profile");
                        console.log('success');
                        console.log(response);
                    },
                    /**
                     * Error callback
                     * @param response
                     */
                    function (response) {
                        console.log('error');
                        console.log(response);
                    }
                )
            }
        });
})();