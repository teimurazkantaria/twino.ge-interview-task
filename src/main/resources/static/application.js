(function(){
    'use strict';

    angular
        .module('twino', [
            'ngRoute',
            'ngResource',
            'ngLoadingSpinner',
            '720kb.datepicker'
        ])
        .constant('settings', {
            baseUrl: '',
            apiUrl: 'http://localhost:8080/api'
        })
        .config(['$routeProvider', 'settings', '$httpProvider',
            function($routeProvider) {
                $routeProvider
                    .when('/', {
                        templateUrl: 'view/signup/index.html',
                        controller: 'SignupCtrl'
                    })
                    .when('/clients', {
                        templateUrl: 'view/clients/index.html',
                        controller: 'ClientsCtrl'
                    })
                    .when('/profile', {
                        templateUrl: 'view/profile/index.html',
                        controller: 'ProfileCtrl'
                    })

                    .when('/signin', {
                        templateUrl: 'view/signin/index.html',
                        controller: 'SigninCtrl'
                    });
            }])
        // Handle Http Response Errors Globally
        .factory('httpResponseInterceptor',['$q','$location', 'app',function($q,$location, app){
            return {
                response: function(response){
                    return response || $q.when(response);
                },
                responseError: function(rejection) {
                    app.error.message = "Error occurred. ";
                    if (rejection.data) {
                        if (rejection.data.message)
                            app.error.message = rejection.data.message;
                        if (rejection.data.exceptionId)
                            app.error.message +=  "  (Error id: "+rejection.data.exceptionId;
                        if (rejection.data.errorCode)
                            app.error.message +=  ", Error code: "+rejection.data.errorCode;
                        if (rejection.data.exceptionId || rejection.data.errrCode)
                            app.error.message += ")";
                    }
                    return $q.reject(rejection);
                }
            }
        }])
        .config(['$httpProvider',function($httpProvider) {
            $httpProvider.interceptors.push('httpResponseInterceptor');
        }])
        .run(function($rootScope, app) {
            $rootScope.app = app;
        });

})();