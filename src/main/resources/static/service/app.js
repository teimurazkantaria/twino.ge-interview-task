(function(){
    'use strict';

    angular
        .module("twino")
        .factory('app', ["$location", "$rootScope", app]);

    function app($location, $rootScope ) {

        $rootScope.$on('$routeChangeStart', function(angularEvent,next,current) {
            service.error.message = "";
        });

        var service = {
            activeController: "clients",

            apiConsumer: null,

            defineTopMenuItemClass: function(controller) {
                return this.activeController == controller ? "active" : "";
            },

            getCurrentDate: function() {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0

                var yyyy = today.getFullYear();
                if(dd < 10) {
                    dd= '0' + dd
                }
                if(mm < 10){
                    mm = '0' + mm
                }
                return yyyy + '-' + mm + '-' + dd;
            },

            convertDateToApiForm: function(date) {
                return date.split("/").reverse().join("-");
            },

            error: {
                message: ""
            }
        };

        return service;
    }

})();
