package twino.domain;

import org.joda.time.DateTime;
import org.junit.Test;
import twino.AbstractUnitTestCase;
import twino.domain.error.DomainException;
import twino.infrastructure.InMemoryDomainConfigurationProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClientTest extends AbstractUnitTestCase {

    private DomainConfigurationProvider configurationProvider = new InMemoryDomainConfigurationProvider();

    @Test(expected = IllegalArgumentException.class)
    public void calculateAge_nullAsToDateParam_throwException() {
        Client client = new Client.Builder(configurationProvider).build();
        client.calculateAge(null);
    }

    @Test
    public void calculateAge_shouldCalculateAgeProperly() {
        DateTime birthDate = DateTime.parse("1985-06-25");
        DateTime toDate = DateTime.parse("2016-12-13");
        Integer expectedAge = 31;
        Client client = new Client.Builder(clientsFixture.getCaesarClient()).birthDate(birthDate).build();

        Integer actualAge = client.calculateAge(toDate);

        assertEquals(expectedAge, actualAge);
    }

    @Test
    public void calculateAge_toDateLessThanBirthDate_throwException() {
        DateTime birthDate = DateTime.parse("1985-06-25");
        DateTime toDate = DateTime.parse("1984-12-13");
        Client client = new Client.Builder(clientsFixture.getCaesarClient()).birthDate(birthDate).build();

        boolean exceptionGot = false;
        try {
            client.calculateAge(toDate);
        } catch (DomainException e) {
            exceptionGot = true;
            assertEquals(DomainErrorCode.AGE_CALC_TO_DATE_LESS_THAN_BIRTH_DATE, e.getErrorCode());
        }
        assertTrue(DomainException.class + " exception expected", exceptionGot);
    }

    @Test
    public void changeCreditLimit_shouldChangeIt() {
        Client client = clientsFixture.getCaesarClient();

        client.changeCreditLimit(getSomeUsd(2000));
        assertEquals(getSomeUsd(2000), client.getCreditLimit());
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeCreditLimit_nullAsCreditLimitParam_throwException() {
        Client client = new Client.Builder(configurationProvider).build();

        client.changeCreditLimit(null);
    }
}