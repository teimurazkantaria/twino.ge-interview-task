package twino.domain;

import org.junit.Test;
import twino.AbstractUnitTestCase;

public class CurrencyTest extends AbstractUnitTestCase {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullAsCodeParam_throwException() {
        new Currency(null);
    }
}