package twino.domain;

import org.junit.Test;
import twino.AbstractUnitTestCase;
import twino.domain.error.DomainException;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MoneyTest extends AbstractUnitTestCase {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullAsAmountParam_throwException() {
        new Money(null, new Currency("USD"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullAsCurrencyParam_throwException() {
        new Money(new BigDecimal(1000), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_nullAsMoneyParam_throwException() {
        getSomeUsd(1).add(null);
    }

    @Test(expected = DomainException.class)
    public void add_moneyWithDifferentCurrencies_throwException() {
        getSomeUsd(1).add(getSomeGel(2));
    }

    @Test
    public void add_shouldAddProperly() {
        Money result = getSomeUsd(10).add(getSomeUsd(90));

        assertEquals(getSomeUsd(100), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtract_nullAsMoneyParam_throwException() {
        getSomeUsd(1).subtract(null);
    }

    @Test(expected = DomainException.class)
    public void subtract_moneyWithDifferentCurrencies_throwException() {
        getSomeUsd(2).subtract(getSomeGel(1));
    }

    @Test
    public void subtract_shouldSubtractProperly() {
        Money result = getSomeUsd(100).subtract(getSomeUsd(90));

        assertEquals(getSomeUsd(10), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiply_nullAsMoneyParam_throwException() {
        getSomeUsd(1).multiply(null);
    }

    @Test(expected = DomainException.class)
    public void multiply_moneyWithDifferentCurrencies_throwException() {
        getSomeUsd(1).multiply(getSomeGel(2));
    }

    @Test
    public void multiply_shouldMultiplyProperly() {
        Money result = getSomeUsd(10).multiply(getSomeUsd(90));

        assertEquals(getSomeUsd(900), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divide_nullAsMoneyParam_throwException() {
        getSomeUsd(20).divide(null);
    }

    @Test(expected = DomainException.class)
    public void divide_moneyWithDifferentCurrencies_throwException() {
        getSomeUsd(20).divide(getSomeGel(2));
    }

    @Test
    public void divide_shouldDivideProperly() {
        Money result = getSomeUsd(900).divide(getSomeUsd(90));

        assertEquals(getSomeUsd(10), result);
    }
}