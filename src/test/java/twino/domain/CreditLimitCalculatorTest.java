package twino.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import twino.AbstractUnitTestCase;
import twino.api.error.ValidationException;
import twino.domain.error.DomainException;
import twino.infrastructure.InMemoryDomainConfigurationProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreditLimitCalculatorTest extends AbstractUnitTestCase {

    private CreditLimitCalculator creditLimitCalculator;

    private InMemoryDomainConfigurationProvider configurationProvider;

    private DomainConfiguration configuration;

    @Before
    public void setUpCalculator() {
        configurationProvider = new InMemoryDomainConfigurationProvider();
        creditLimitCalculator = new CreditLimitCalculator(configurationProvider);
        configuration = configurationProvider.getConfig();
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculate_nullAsAgeParam_throwException() {
        creditLimitCalculator.calculate(null, getSomeUsd(100), getSomeUsd(200));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculate_nullAsMonthlySalaryParam_throwException() {
        creditLimitCalculator.calculate(20, null, getSomeUsd(200));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculate_nullAsLiabilitiesParam_throwException() {
        creditLimitCalculator.calculate(20, getSomeUsd(100), null);
    }

    @Test
    public void calculate_salaryAndLiabilitiesCurrenciesDoNotMatch_throwException() {
        boolean exceptionGot = false;
        try {
            creditLimitCalculator.calculate(21, getSomeUsd(100), getSomeGel(200));
        } catch (DomainException e) {
            exceptionGot = true;
            assertEquals(DomainErrorCode.CURRENCIES_MISMATCH, e.getErrorCode());
        }

        assertTrue(DomainErrorCode.class + " exception excpected", exceptionGot);
    }

    @Test(expected = DomainException.class)
    public void calculate_configIsNull_throwException() {
        InMemoryDomainConfigurationProvider providerMock = mock(InMemoryDomainConfigurationProvider.class);
        CreditLimitCalculator calculator = new CreditLimitCalculator(providerMock);
        when(providerMock.getConfig()).thenReturn(null);

        calculator.calculate(20, getSomeUsd(100), getSomeUsd(200));
    }

    @Test
    public void calculate_ageLessThenMin_throwException() {
        boolean exceptionGot = false;
        try {
            creditLimitCalculator.calculate(configuration.getClientMinAge() - 1, getSomeUsd(100), getSomeUsd(200));
        } catch (DomainException e) {
            exceptionGot = true;
        }
        Assert.assertTrue(ValidationException.class + " exception expected", exceptionGot);
    }

    @Test
    public void calculate_ageGreaterThanMaxCreditLimitAge_zero() {
        Money creditLimit = creditLimitCalculator.calculate(configuration.getClientMaxAgeForCreditLimit() + 1, getSomeUsd(100), getSomeUsd(200));

        assertEquals(getSomeUsd(0), creditLimit);
    }

    @Test
    public void calculate_ageOk_properCreditLimit() {
        Money expectedCreditLimit = getSomeUsd(4810);

        Money actualCreditLimit = creditLimitCalculator.calculate(30, getSomeUsd(2000), getSomeUsd(190));

        assertEquals(expectedCreditLimit, actualCreditLimit);
    }
}