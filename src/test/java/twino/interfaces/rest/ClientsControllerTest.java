package twino.interfaces.rest;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import twino.AbstractUnitTestCase;
import twino.api.ClientProfileService;
import twino.api.ClientViewerService;
import twino.domain.Client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ClientsControllerTest extends AbstractUnitTestCase {

    @Mock
    private ClientProfileService clientProfileService;

    @Mock
    private ClientViewerService clientViewerService;

    @InjectMocks
    private ClientsController clientsController;

    @Test
    public void clientControllerCreation_oneOfParamNull_throwException() {
        assertThatThrownBy(() -> new ClientsController(null, clientViewerService))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Client profile service must not be null");

        assertThatThrownBy(() -> new ClientsController(clientProfileService, null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Client viewer service must not be null");
    }

    @Test
    public void get_shouldReturnClientById() {
        Client expectedClient = clientsFixture.getCaesarClient();
        when(clientViewerService.getClient(1L)).thenReturn(expectedClient);

        Client actualClient = clientsController.get(1L);

        assertEquals(expectedClient, actualClient);
    }

    @Test
    public void changeName_shouldChangeClientName() {
        Client expectedClient = clientsFixture.getCaesarClient();
        when(clientViewerService.getClient(1L)).thenReturn(expectedClient);

        Client actualClient = clientsController.get(1L);

        assertEquals(expectedClient, actualClient);
    }

}