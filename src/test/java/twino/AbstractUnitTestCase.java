package twino;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import twino.domain.Currency;
import twino.domain.Money;
import twino.fixture.ClientsFixture;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractUnitTestCase {

    protected Money getSomeUsd(int amount) {
        return new Money(new BigDecimal(amount), Currency.USD);
    }

    protected Money getSomeGel(int amount) {
        return new Money(new BigDecimal(amount), Currency.GEL);
    }

    protected ClientsFixture clientsFixture;

    @Before
    public void setUp() {
        clientsFixture = new ClientsFixture();
    }
}
