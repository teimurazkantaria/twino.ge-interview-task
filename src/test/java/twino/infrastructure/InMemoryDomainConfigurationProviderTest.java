package twino.infrastructure;

import org.junit.Test;
import twino.AbstractUnitTestCase;
import twino.domain.DomainConfiguration;

import static org.junit.Assert.assertNotNull;

public class InMemoryDomainConfigurationProviderTest extends AbstractUnitTestCase {

    @Test
    public void getConfig_shouldCreateConfiguration() {
        InMemoryDomainConfigurationProvider provider = new InMemoryDomainConfigurationProvider();
        DomainConfiguration domainConfiguration = provider.getConfig();

        assertNotNull("non-null domainConfiguration expected", domainConfiguration);
        assertNotNull("non-null clientMinAge expected", domainConfiguration.getClientMinAge());
        assertNotNull("non-null clientMaxAgeForCreditLimit expected", domainConfiguration.getClientMaxAgeForCreditLimit());
    }
}