package twino.fixture;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import twino.domain.Client;
import twino.domain.Currency;
import twino.domain.DomainConfigurationProvider;
import twino.domain.Money;
import twino.infrastructure.InMemoryDomainConfigurationProvider;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class ClientsFixture {

    private List<Client> clients;

    private Client caesarClient;
    private Client hannibalClient;

    private DomainConfigurationProvider configurationProvider = new InMemoryDomainConfigurationProvider();

    public ClientsFixture() {
        clients = new ArrayList<>();

        caesarClient = new Client.Builder(configurationProvider)
                .id(1479304267L)
                .name("caesar")
                .createdAt(DateTime.parse("2016-08-25T12:38:00"))
                .birthDate(DateTime.parse("1980-08-20T12:00:00"))
                .phoneNumber("phone1")
                .monthlySalary(new Money(new BigDecimal(9999), new Currency("USD")))
                .remainingLiabilities(new Money(new BigDecimal(100), new Currency("USD")))
                .build();
        clients.add(caesarClient);

        hannibalClient = new Client.Builder(configurationProvider)
                .id(1480271187L)
                .name("hannibal")
                .createdAt(DateTime.parse("2016-08-25T12:38:00"))
                .birthDate(DateTime.parse("1980-08-20T12:00:00"))
                .phoneNumber("phone2")
                .monthlySalary(new Money(new BigDecimal(9998), new Currency("USD")))
                .remainingLiabilities(new Money(new BigDecimal(200), new Currency("USD")))
                .build();
        clients.add(hannibalClient);
    }

    public List<Client> getClients() {
        return clients;
    }

    public Client getCaesarClient() {
        return caesarClient;
    }

    public Client getHannibalClient() {
        return hannibalClient;
    }
}
