package twino.api.internal;

import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import twino.AbstractUnitTestCase;
import twino.api.dto.SignUpNewClientDto;
import twino.api.event.NewClientSignedUpEvent;
import twino.domain.*;
import twino.infrastructure.InMemoryDomainConfigurationProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class DefaultClientRegistrationServiceTest extends AbstractUnitTestCase {

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private CreditLimitCalculator creditLimitCalculator;

    @Mock
    private DomainConfigurationProvider configurationProvider;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @InjectMocks
    private DefaultClientRegistrationService clientRegistrationService;

    @Test(expected = IllegalArgumentException.class)
    public void signUpNewClient_nullAsClientParam_throwException() {
        clientRegistrationService.signUpNewClient(null);
    }

    @Test
    public void signUpNewClient_shouldCalculateCreditLimit() {
        SignUpNewClientDto dto = signUpNewClientDto();
        Money salary = dto.getMonthlySalary();
        Money liabilities = dto.getRemainingLiabilities();
        DateTime birthDate = dto.getBirthDate();

        when(creditLimitCalculator.calculate(any(), any(), any())).thenReturn(getSomeUsd(1000));
        when(configurationProvider.getConfig()).thenReturn((new InMemoryDomainConfigurationProvider().getConfig()));
        clientRegistrationService.signUpNewClient(dto);

        verify(creditLimitCalculator, times(1)).calculate(any(Integer.class), eq(salary), eq(liabilities));
    }

    @Test
    public void signUpNewClient_shouldStoreNewClientWithCreditLimitAndReturnClient() {
        Money creditLimit = getSomeUsd(1000);
        Money salary = getSomeUsd(2000);
        Money liabilities = getSomeUsd(190);
        DateTime birthDate = new DateTime("1985-06-25");
        SignUpNewClientDto dto = new SignUpNewClientDto.Builder()
                .birthDate(birthDate)
                .monthlySalary(salary)
                .remainingLiabilities(liabilities)
                .name("John")
                .phoneNumber("+995111111111")
                .build();

        Client expectedClient = new Client.Builder(new InMemoryDomainConfigurationProvider())
                .id(1L)
                .name(dto.getName())
                .phoneNumber(dto.getPhoneNumber())
                .birthDate(dto.getBirthDate())
                .monthlySalary(dto.getMonthlySalary())
                .remainingLiabilities(dto.getRemainingLiabilities())
                .name(dto.getName())
                .phoneNumber(dto.getPhoneNumber())
                .build();

        when(creditLimitCalculator.calculate(anyInt(), eq(salary), eq(liabilities))).thenReturn(creditLimit);
        ArgumentCaptor<Client> clientInMethod = ArgumentCaptor.forClass(Client.class);
        when(clientRepository.save(any(Client.class))).thenReturn(expectedClient);
        when(configurationProvider.getConfig()).thenReturn((new InMemoryDomainConfigurationProvider().getConfig()));

        Client createdClient = clientRegistrationService.signUpNewClient(dto);

        assertNotNull(createdClient);
        // Check client stored
        verify(clientRepository, times(1)).save(clientInMethod.capture());
        // with credit limit
        assertEquals(creditLimit, clientInMethod.getValue().getCreditLimit());

        assertEquals(expectedClient, createdClient);
        assertNotNull(clientInMethod.getValue().getName());
        assertNotNull(clientInMethod.getValue().getPhoneNumber());
        assertNotNull(clientInMethod.getValue().getBirthDate());
        assertNotNull(clientInMethod.getValue().getMonthlySalary());
        assertNotNull(clientInMethod.getValue().getRemainingLiabilities());
        assertNotNull(clientInMethod.getValue().getCreditLimit());
    }

    @Test
    public void signUpNewClient_shouldPublishEventOnSuccess() {
        SignUpNewClientDto dto = signUpNewClientDto();
        when(creditLimitCalculator.calculate(any(), any(), any())).thenReturn(getSomeUsd(1000));
        when(clientRepository.save(any(Client.class))).thenReturn(clientsFixture.getCaesarClient());
        when(configurationProvider.getConfig()).thenReturn((new InMemoryDomainConfigurationProvider().getConfig()));

        Client client = clientRegistrationService.signUpNewClient(dto);
        assertNotNull(client);

        verify(eventPublisher, times(1)).publishEvent(new NewClientSignedUpEvent(client));
    }

    private SignUpNewClientDto signUpNewClientDto() {
        return new SignUpNewClientDto.Builder()
                .monthlySalary(getSomeUsd(9999))
                .birthDate(DateTime.parse("1980-10-10"))
                .name("Name1")
                .phoneNumber("Phone3")
                .remainingLiabilities(getSomeUsd(100))
                .build();
    }
}